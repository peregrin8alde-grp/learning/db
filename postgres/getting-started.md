# Getting Started

https://www.postgresql.org/docs/12/tutorial-start.html

## Creating a Database

```
createdb -h postgres -U postgres

createdb -h postgres -U postgres mydb
```

```
dropdb -h postgres -U postgres mydb
```

## Accessing a Database

```
psql -h postgres -U postgres mydb
```

```
psql (13.2 (Debian 13.2-1.pgdg100+1))
Type "help" for help.

mydb=#
```

```
mydb=# SELECT version();
                                                     version                                                      
------------------------------------------------------------------------------------------------------------------
 PostgreSQL 13.2 (Debian 13.2-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
(1 row)

mydb=# SELECT current_date;
 current_date 
--------------
 2021-04-18
(1 row)

mydb=# SELECT 2 + 2;
 ?column? 
----------
        4
(1 row)

\q
```