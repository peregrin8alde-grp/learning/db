# Docker を使った利用方法

https://hub.docker.com/_/postgres

```
docker pull postgres
```

```
# get the default config
mkdir -p conf

docker run \
  -i \
  -u 1000:1000 \
  --rm \
  -v $PWD/conf:/sample \
  postgres \
    bash -c 'cp /usr/share/postgresql/${PG_MAJOR}/*.sample /sample/'

cp conf/postgresql.conf.sample conf/postgresql.conf
cp conf/pg_hba.conf.sample conf/pg_hba.conf

sed -i -e "s/@authcomment@//g" conf/pg_hba.conf
sed -i -e "s/@remove-line-for-nolocal@//g" conf/pg_hba.conf
sed -i -e "s/@authmethodlocal@/trust/g" conf/pg_hba.conf
sed -i -e "s/@authmethodhost@/trust/g" conf/pg_hba.conf

tee -a conf/pg_hba.conf <<"EOS"

# all trust
host all all all trust
EOS
```

```
docker run \
  -d \
  --name postgres \
  -e POSTGRES_PASSWORD=postgres \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v $(pwd)/data:/var/lib/postgresql/data \
  -v "$PWD/conf/postgresql.conf":/etc/postgresql/postgresql.conf \
  -v "$PWD/conf/pg_hba.conf":/etc/postgresql/pg_hba.conf \
  postgres \
    -c 'config_file=/etc/postgresql/postgresql.conf' \
    -c 'hba_file=/etc/postgresql/pg_hba.conf'

docker network create postgres_nw
docker network connect postgres_nw postgres

docker run -it --rm --network postgres_nw postgres psql -h postgres -U postgres
```

## Initialization scripts

`/docker-entrypoint-initdb.d` 配下に以下のいずれかの拡張子のファイルを格納しておくと、初期処理として実行してくれる。
（データディレクトリが空の初期状態のときのみ）

- `*.sql`
- `*.sql.gz`
- `*.sh`

```
sudo rm -rf "$PWD/data"

docker run \
  -d \
  --name postgres \
  -e POSTGRES_PASSWORD=postgres \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v "$PWD/data:/var/lib/postgresql/data" \
  -v "$PWD/conf/postgresql.conf":/etc/postgresql/postgresql.conf \
  -v "$PWD/conf/pg_hba.conf":/etc/postgresql/pg_hba.conf \
  -v "$PWD/initd":/docker-entrypoint-initdb.d \
  postgres \
    -c 'config_file=/etc/postgresql/postgresql.conf' \
    -c 'hba_file=/etc/postgresql/pg_hba.conf'
```

```
docker exec -it postgres psql -U postgres

postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 docker    |                                                            | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

## compose

```
cd compose
docker-compose up -d

docker-compose stop
docker-compose down --volumes
```
