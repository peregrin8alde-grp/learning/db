# Docker Compose を利用した環境構築

## 前準備

- Docker インストール
- Docker Compose インストール

## 構成

MariaDB 10.5, [mariadb:10.5](https://hub.docker.com/_/mariadb)

## 操作

```
docker-compose -p mariadb up -d

docker-compose -p mariadb ps

docker-compose -p mariadb logs

docker-compose -p mariadb exec db mysql -hdb -uroot -proot
docker-compose -p mariadb exec db mysql -hdb -uroot -proot -e "SHOW DATABASES"



docker-compose -p mariadb stop

docker-compose -p mariadb restart db

docker-compose -p mariadb down --volumes
```

